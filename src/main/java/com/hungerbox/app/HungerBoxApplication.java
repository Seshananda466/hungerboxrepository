package com.hungerbox.app;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.hungerbox.app.config.RibbonConfiguration;

@SpringBootApplication

 @EnableEurekaClient 
@EnableFeignClients
@EnableScheduling
@RibbonClient(value="Banking-Service",configuration=RibbonConfiguration.class)
public class HungerBoxApplication implements CommandLineRunner {
	
	@Autowired
	JobLauncher jobLauncher;
	
	@Autowired
	JobOperator jobOperator;
	
	@Autowired
	Job job;

	public static void main(String[] args) {
		SpringApplication.run(HungerBoxApplication.class, args);
	}
	
	@Scheduled(fixedDelay=30000)
	public void doJob() {
		JobParameters parameters = new JobParametersBuilder()
				.addString("JobId", String.valueOf(System.currentTimeMillis()))
				.addString("JobName", job.getName()).toJobParameters();
		try {
			jobLauncher.run(job, parameters);
			
		}
		catch(JobExecutionAlreadyRunningException e) {
			e.printStackTrace();
			
		} catch (JobRestartException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JobInstanceAlreadyCompleteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JobParametersInvalidException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
