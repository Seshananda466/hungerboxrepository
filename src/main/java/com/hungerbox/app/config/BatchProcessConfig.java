package com.hungerbox.app.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import com.hungerbox.app.entity.Vendor;
import com.hungerbox.app.processor.FileArchiveTasklet;
import com.hungerbox.app.processor.VendorJobListener;
import com.hungerbox.app.processor.VendorMapper;
import com.hungerbox.app.processor.VendorWriter;

@Configuration
@EnableBatchProcessing
public class BatchProcessConfig {

	public static final Logger logger = LoggerFactory.getLogger(BatchProcessConfig.class);

	@Autowired
	DataSource dataSource;

	@Autowired
	EntityManagerFactory entityManager;

	@Autowired
	private JobBuilderFactory jobs;

	@Autowired
	private StepBuilderFactory stepfFactory;
	@Value("classpath:/vendor*.csv")
	private Resource[] vendorInput;

	@Bean
	public MultiResourceItemReader<Vendor> multiResourceItemReader() {
		MultiResourceItemReader<Vendor> reader = new MultiResourceItemReader<Vendor>();
		reader.setDelegate(reader());
		reader.setResources(vendorInput);
		reader.setStrict(false);
		return reader;

	}

	@Bean
	public FlatFileItemReader<Vendor> reader() {
		FlatFileItemReader<Vendor> vendorReader = new FlatFileItemReader<Vendor>();
		DelimitedLineTokenizer delimiter = new DelimitedLineTokenizer();
		String[] tokens = {"vendorid", "vendorname", "vendoremail", "vendoraccountnumber", "vendordescription",
				"vendorphonenumber", "vendoraddress" };
		delimiter.setNames(tokens);
		delimiter.setDelimiter(",");
		vendorReader.setStrict(false);
		DefaultLineMapper<Vendor> lineMapper = new DefaultLineMapper<Vendor>();
		lineMapper.setLineTokenizer(delimiter);
		VendorMapper mapper = new VendorMapper();
		lineMapper.setFieldSetMapper(mapper);
		vendorReader.setLineMapper(lineMapper);
		vendorReader.setLinesToSkip(0);
		return vendorReader;

	}

	@Bean
	public VendorWriter<Vendor> writer() {
		return new VendorWriter<Vendor>();
	}

	@Bean
	public JdbcBatchItemWriter<Vendor> jdbcwriter() {
		JdbcBatchItemWriter<Vendor> dbWriter = new JdbcBatchItemWriter<Vendor>();
		dbWriter.setDataSource(dataSource);
		dbWriter.setSql(
				"INSERT INTO VEENDOR_ID,VENDOR_NAME,VENDOR_EMAIL,VENDOR_ACCOUNT_NUMBER,VENDOR_DESCRIPTION,VENDOR_PHONENUMBER,VENDOR_ADDRESS) VALUES(:vendorid, :vendorname, :vendoremail, :vendoraccountnumber, :vendordescription, :vendorphonenumber, :vendoraddress");
		dbWriter.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Vendor>());
		return dbWriter;

	}

	@Bean
	public JpaItemWriter<Vendor> jpaWriter() {
		JpaItemWriter<Vendor> vendorWriter = new JpaItemWriter<Vendor>();
		vendorWriter.setEntityManagerFactory(entityManager);
		return vendorWriter;

	}

	@Bean
	public Job readVendorCSV() {
		return jobs.get("readVendorCSV").incrementer(new RunIdIncrementer()).listener(new VendorJobListener())
				.flow(step1()).next(step2()).end().build();

	}

	@Bean
public Step step1() {
		// TODO Auto-generated method stub
		return stepfFactory.get("step1").<Vendor, Vendor>chunk(5).reader(multiResourceItemReader()).writer(jpaWriter())
				.build();
	}

	@Bean
	public Step step2() {
		// TODO Auto-generated method stub
		FileArchiveTasklet tasklet = new FileArchiveTasklet();
		tasklet.setResources(vendorInput);
		return stepfFactory.get("step2").tasklet(tasklet).build();
	}

}
