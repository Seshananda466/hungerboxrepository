package com.hungerbox.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hungerbox.app.dto.FoodItemDto;
import com.hungerbox.app.dto.FoodItemRequestDto;
import com.hungerbox.app.exception.FoodItemNotFoundException;
import com.hungerbox.app.service.FoodItemService;

import io.swagger.annotations.Api;

@RestController
@Api(value="getOrders",description="Getting Orders List",tags={"getOrders"})
public class FoodItemController {

	@Autowired
	FoodItemService foodItemService;

	@GetMapping(value = "/getfoodItems")
	@ResponseStatus(HttpStatus.ACCEPTED)
	
	public List<FoodItemRequestDto> getFoodItemDtoByFoodName(@RequestParam String foodName, @RequestParam int pageNumber, @RequestParam int pageSize) throws FoodItemNotFoundException{
	
		return foodItemService.findByfoodNameContains(foodName,pageNumber,pageSize);
	}
}
