package com.hungerbox.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hungerbox.app.entity.OrderItems;
import com.hungerbox.app.exception.OrderDetailsNotFoundException;
import com.hungerbox.app.service.OrderItemsService;

import io.swagger.annotations.Api;

@RestController
@Api(value="getOrders",description="Getting Orders List",tags={"getOrders"})
public class OrderItemsController {
	@Autowired 
	OrderItemsService orderItemsService;

	@GetMapping(value = "/getOrdersByPaging")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<OrderItems>> getOrders(@RequestParam(defaultValue="0") Integer pageNo,@RequestParam(defaultValue="5") Integer pageSize, @RequestParam(defaultValue="orderId") String sortByDesc) throws OrderDetailsNotFoundException{
		List<OrderItems> list=orderItemsService.getOrderItemsByDesc(pageNo,pageSize,sortByDesc);
		return new ResponseEntity<List<OrderItems>>(list,new HttpHeaders(),HttpStatus.OK);
	}
}

