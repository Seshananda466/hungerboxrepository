package com.hungerbox.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hungerbox.app.dto.FundTransactionResponse;
import com.hungerbox.app.dto.UserOrderDto;
import com.hungerbox.app.exception.UserNotFoundException;
import com.hungerbox.app.feignclient.FundTransactionClient;
import com.hungerbox.app.service.UserOrderService;

import io.swagger.annotations.Api;



@RestController
@Api(value="createOrder",description="Creating order",tags={"createOrder"})
class UserOrderController {

	@Autowired
	UserOrderService userOrderService;
	@Autowired
	FundTransactionClient fundTransactionClient;
	

	@GetMapping("/test")
	public String getPortNo() {
		return fundTransactionClient.getPortNo();
	}

	@PostMapping(value = "/orderFood")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<String> createOrder(@RequestBody UserOrderDto userOrderDto) {
		 
			userOrderService.createOrder(userOrderDto);
		
		return new ResponseEntity<>("Order Placed Successfully",HttpStatus.CREATED);
	}

	@GetMapping(value = "/getOrderDetails")
	@ResponseStatus(HttpStatus.OK)

	public List<UserOrderDto> getOrderList(@RequestParam int userId, @RequestParam int pageNumber, @RequestParam int pageSize) throws UserNotFoundException{
		
		return userOrderService.getOrderList(userId,pageNumber,pageSize);

	}

	@PostMapping(value = "/bank/fundTransfer")
	public ResponseEntity<String> fundTransfer(@RequestBody FundTransactionResponse fundTransactionResponse) {
		System.out.println("fundTransactionResponse:"+fundTransactionResponse.getToAccountNumber());
		fundTransactionClient.fundTransfer(fundTransactionResponse);
		return new ResponseEntity<>("Tranfer Successfully", HttpStatus.OK);

	}

	@GetMapping(value = "/transactionHistory")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<FundTransactionResponse>> getTransactions(@RequestParam("accountNumber")Long accountNumber,@RequestParam(defaultValue="0") Integer pageNo,@RequestParam(defaultValue="10") Integer pageSize, @RequestParam(defaultValue="transactionId") String sortByDesc){
		List<FundTransactionResponse> list=(List<FundTransactionResponse>) fundTransactionClient.getTransactions(accountNumber,pageNo,pageSize,sortByDesc);
		return new ResponseEntity<List<FundTransactionResponse>>(list,new HttpHeaders(),HttpStatus.OK);
	}
}
