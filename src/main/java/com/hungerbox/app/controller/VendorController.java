package com.hungerbox.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hungerbox.app.dto.VendorDto;
import com.hungerbox.app.entity.Vendor;
import com.hungerbox.app.exception.VendorNotFoundException;
import com.hungerbox.app.service.VendorService;

import io.swagger.annotations.Api;

@RestController
@Api(value="getVendorByVendorName",description="Getting Vendor by Vendor Name",tags={"getVendorByVendorName"})
public class VendorController {

	@Autowired
	VendorService vendorService;

	@GetMapping(value = "/getVendor")
	@ResponseStatus(HttpStatus.ACCEPTED)

	public ResponseEntity<List<VendorDto>> getVendorByVendorName(@RequestParam String vendorName, @RequestParam int pageNumber, @RequestParam int pageSize) throws VendorNotFoundException{
		List<VendorDto> list =vendorService.findByvendorNameContains(vendorName,pageNumber,pageSize);
		return new ResponseEntity<List<VendorDto>>(new HttpHeaders(), HttpStatus.OK);
	}

}
