package com.hungerbox.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hungerbox.app.entity.VendorItems;
import com.hungerbox.app.service.VendorItemsService;

import io.swagger.annotations.Api;

@RestController
@Api(value="addVendorItems",description="Adding Vendor Items",tags={"addVendorItems"})
public class VendorItemsController {

	@Autowired
	VendorItemsService vendorItemsService;

	@PostMapping(value = "/selectVendorItem")
	@ResponseStatus(HttpStatus.CREATED)

	public ResponseEntity<String> addVendorItems(@RequestBody VendorItems vendorItems) {
		return new ResponseEntity<>(vendorItemsService.addVendorItems(vendorItems), HttpStatus.CREATED);

	}
}
