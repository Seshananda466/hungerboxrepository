package com.hungerbox.app.dto;

import javax.validation.constraints.NotNull;



public class FoodItemDto {
	
	@NotNull(message="Food Id Mandatory") 
	private int foodId;
	@NotNull(message="Quantity Mandatory") 
	private int quantity;

	public Integer getFoodId() {
		return foodId;
	}

	public void setFoodId(Integer foodId) {
		this.foodId = foodId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public FoodItemDto(Integer foodId,int quantity) {
		super();
		this.foodId = foodId;
		this.quantity=quantity;

	}

	public FoodItemDto() {
		super();
		// TODO Auto-generated constructor stub
	}

}
