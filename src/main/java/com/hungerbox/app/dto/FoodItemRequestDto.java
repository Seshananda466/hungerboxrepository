package com.hungerbox.app.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class FoodItemRequestDto {
	@NotNull(message = "Food Id Mandatory")
	private Integer foodId;
	@NotEmpty(message = "Vendor Id is Mandatory")
	private String vendorId; 
	@NotEmpty(message = "Food Name is Mandatory")
	private String foodName;
	private String foodDescription;
	@NotNull(message = "Food Price is Mandatory")
	private float foodPrice;
	public Integer getFoodId() {
		return foodId;
	}
	public void setFoodId(Integer foodId) {
		this.foodId = foodId;
	}
	public String getFoodName() {
		return foodName;
	}
	public void setFoodName(String foodName) {
		this.foodName = foodName;
	}
	public String getFoodDescription() {
		return foodDescription;
	}
	public void setFoodDescription(String foodDescription) {
		this.foodDescription = foodDescription;
	}
	public float getFoodPrice() {
		return foodPrice;
	}
	public void setFoodPrice(float foodPrice) {
		this.foodPrice = foodPrice;
	}
	public String getVendorId() {
		return vendorId;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	public FoodItemRequestDto(Integer foodId, String vendorId, String foodName, String foodDescription, float foodPrice) {
		super();
		this.foodId = foodId;
		this.vendorId = vendorId;
		this.foodName = foodName;
		this.foodDescription = foodDescription;
		this.foodPrice = foodPrice;
	}
	public FoodItemRequestDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
