package com.hungerbox.app.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotEmpty;

public class UserOrderDto {
	@NotEmpty(message ="User Id is Mandatory")
	private String userId;
	/* private String foodId; */
	@NotEmpty(message ="Food Item is Mandatory")
	private List<FoodItemDto> foodItems = new ArrayList<FoodItemDto>();
	/* private int quantity; */
	/* private Long accountNumber; */

	public String getUserId() {
		return userId;
	}

	/*
	 * public void setUserId(String userId) { this.userId = userId; }
	 * 
	 * public String getFoodId() { return foodId; }
	 * 
	 * public void setFoodId(String foodId) { this.foodId = foodId; }
	 */

	/*
	 * public int getQuantity() { return quantity; }
	 * 
	 * public void setQuantity(int quantity) { this.quantity = quantity; }
	 */

	/*
	 * public Long getAccountNumber() { return accountNumber; }
	 * 
	 * public void setAccountNumber(Long accountNumber) { this.accountNumber =
	 * accountNumber; }
	 */

	public List<FoodItemDto> getFoodItems() {
		return foodItems;
	}

	public void setFoodItems(List<FoodItemDto> foodItems) {
		this.foodItems = foodItems;
	}

	public UserOrderDto() {
		super();
		// TODO Auto-generated constructor stub
	}

}
