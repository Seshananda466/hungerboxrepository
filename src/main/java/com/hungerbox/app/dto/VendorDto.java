package com.hungerbox.app.dto;

public class VendorDto {
	private String vendorId;
	private String vendorName;
	private String description;
	private String address;
	private String vendorPhoneNum;
	private String vendorEmail;
	private Long vendorAccountNumber;
	
	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getVendorPhoneNum() {
		return vendorPhoneNum;
	}

	public void setVendorPhoneNum(String vendorPhoneNum) {
		this.vendorPhoneNum = vendorPhoneNum;
	}

	public String getVendorEmail() {
		return vendorEmail;
	}

	public void setVendorEmail(String vendorEmail) {
		this.vendorEmail = vendorEmail;
	}

	public VendorDto(String vendorId, String vendorName, String description, String address, String vendorPhoneNum,
			String vendorEmail,Long vendorAccountNumber) {
		super();
		this.vendorId = vendorId;
		this.vendorName = vendorName;
		this.description = description;
		this.address = address;
		this.vendorPhoneNum = vendorPhoneNum;
		this.vendorEmail = vendorEmail;
		this.vendorAccountNumber=vendorAccountNumber;
	}

	public VendorDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getVendorAccountNumber() {
		return vendorAccountNumber;
	}

	public void setVendorAccountNumber(Long vendorAccountNumber) {
		this.vendorAccountNumber = vendorAccountNumber;
	}

}
