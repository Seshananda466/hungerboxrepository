package com.hungerbox.app.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import javax.persistence.Table;

@Entity
@Table(name = "vendoritems")
public class VendorItems {

	@Id
	private String id;

	private String foodId;

	private String vendorId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFoodId() {
		return foodId;
	}

	public void setFoodId(String foodId) {
		this.foodId = foodId;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public VendorItems(String id, String foodId, String vendorId) {
		super();
		this.id = id;
		this.foodId = foodId;
		this.vendorId = vendorId;
	}

	public VendorItems() {
		super();
		// TODO Auto-generated constructor stub
	}

}
