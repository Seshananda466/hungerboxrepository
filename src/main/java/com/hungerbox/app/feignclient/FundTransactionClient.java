package com.hungerbox.app.feignclient;
import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.hungerbox.app.dto.FundTransactionResponse;


@FeignClient(value="Banking-Service",url="http://localhost:8032/demo")
public interface FundTransactionClient {

	@PostMapping(value = "/bank/fundtransfer")
	public ResponseEntity<String> fundTransfer(@RequestBody FundTransactionResponse fundTransactionResponse);

	@GetMapping(value="/transactionHistory")
	public List<FundTransactionResponse> getTransactions(@RequestParam("accountNumber")Long accountNumber,@RequestParam Integer pageNo,@RequestParam Integer pageSize,@RequestParam String sortByDesc);

	/*@PostMapping("/bank/fundtransfer")
	public ResponseEntity<String> fundTransfer(@RequestBody FundTransactionResponse fundTransactionResponse);

	@GetMapping("/transactionHistory")
	public ResponseEntity<List<FundTransactionResponse>> getTransactions(@RequestParam("accountNumber")Long accountNumber,@RequestParam(defaultValue="0") Integer pageNo,@RequestParam(defaultValue="10") Integer pageSize, @RequestParam(defaultValue="transactionId") String sortByDesc);
}*/
	@GetMapping("/test")
	public String getPortNo();
}