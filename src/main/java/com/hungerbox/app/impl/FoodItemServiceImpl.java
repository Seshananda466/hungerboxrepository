package com.hungerbox.app.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.hungerbox.app.dto.FoodItemRequestDto;
import com.hungerbox.app.exception.FoodItemNotFoundException;
import com.hungerbox.app.repository.FoodItemRepository;
import com.hungerbox.app.service.FoodItemService;
import com.hungerbox.app.service.FoodItemServiceHelper;


@Service
public class FoodItemServiceImpl implements FoodItemService {

	@Autowired FoodItemRepository foodItemRepository;
	@Override
	public List<FoodItemRequestDto> findByfoodNameContains(String foodName, int pageNumber, int pageSize) throws FoodItemNotFoundException{

		/*
		 * List<FoodItem> foodMenuList = new ArrayList<FoodItem>(); List<FoodItemDto>
		 * foodMenuDtoList = new ArrayList<FoodItemDto>();
		 * 
		 * Pageable pageable = PageRequest.of(pageNumber, pageSize,
		 * Sort.by(Direction.ASC, "foodName")); foodMenuList =
		 * foodItemRepository.findByfoodNameContains(foodName, pageable);
		 * if(foodMenuList.isEmpty()) { throw new
		 * FoodItemNotFoundException("Food Item Not Found"); }
		 * 
		 * 
		 * 
		 * 
		 * 
		 * for (FoodItem foodMenu : foodMenuList) { FoodItemDto foodMenuDto = new
		 * FoodItemDto(); BeanUtils.copyProperties(foodMenu, foodMenuDto);
		 * foodMenuDtoList.add(foodMenuDto); } return foodMenuDtoList; }
		 */
	
	Pageable pageble = PageRequest.of(pageNumber, pageSize,Sort.by(Direction.ASC, "foodName"));
	if (pageble.getPageSize() == 0) {
		throw new FoodItemNotFoundException("Food Item Not Found");
	}
	return foodItemRepository.findByfoodNameContains(foodName, pageble).stream().map(FoodItemServiceHelper::entityToDto)
			.collect(Collectors.toList());
	}
}

