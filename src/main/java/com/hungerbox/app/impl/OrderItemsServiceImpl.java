package com.hungerbox.app.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.hungerbox.app.entity.OrderItems;
import com.hungerbox.app.exception.OrderDetailsNotFoundException;
import com.hungerbox.app.repository.OrderItemsRepository;
import com.hungerbox.app.service.OrderItemsService;
@Service
public class OrderItemsServiceImpl implements OrderItemsService{

	@Autowired OrderItemsRepository orderItemsRepository;

	@Override
	public List<OrderItems> getOrderItemsByDesc(Integer pageNo, Integer pageSize, String sortByDesc) throws OrderDetailsNotFoundException {
		Pageable paging=PageRequest.of(pageNo, pageSize,Sort.by("orderId").descending());
		if(paging.getPageSize()==0) {
			throw new OrderDetailsNotFoundException("Order Details Not Found");
		}
		return orderItemsRepository.findAll(paging).stream().collect(Collectors.toList());
		// TODO Auto-generated method stub
		/*Pageable paging=PageRequest.of(pageNo, pageSize,Sort.by("orderId").descending());
		List<OrderItems> List = new ArrayList<OrderItems>();
		Page<OrderItems> pagesResult=orderItemsRepository.findAll(paging);
		if(pagesResult.hasContent()) {
			List =  pagesResult.getContent();
			return List;
			}else {
				throw new OrderDetailsNotFoundException("Details Not Found");
			}*/
	}
}
