package com.hungerbox.app.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hungerbox.app.dto.FoodItemDto;
import com.hungerbox.app.dto.UserOrderDto;
import com.hungerbox.app.entity.FoodItem;
import com.hungerbox.app.entity.OrderItems;
import com.hungerbox.app.entity.UserOrder;
import com.hungerbox.app.exception.UserNotFoundException;
import com.hungerbox.app.feignclient.FundTransactionClient;
import com.hungerbox.app.repository.FoodItemRepository;
import com.hungerbox.app.repository.OrderItemsRepository;
import com.hungerbox.app.repository.UserOrderRepository;
import com.hungerbox.app.repository.VendorRepository;
import com.hungerbox.app.service.UserOrderService;

@Service
public class UserOrderServiceImpl implements UserOrderService {
	
	@Autowired
	UserOrderRepository userOrderRepository;
	@Autowired
	OrderItemsRepository orderItemsRepository;
	@Autowired
	FoodItemRepository foodItemRepository;
	@Autowired
	VendorRepository vendorRepository;
	@Autowired
	FundTransactionClient fundTransactionClient;
	UserOrder userOrder = new UserOrder();

	@Override
	@Transactional
	
	public String createOrder(UserOrderDto userOrderDto) {
		// TODO Auto-generated method stub
		
		
		double totalprice = 0;
		String userId = userOrderDto.getUserId();
		Date userDate = Calendar.getInstance().getTime();
		/* userOrderRepository.save(userOrder); */

		for (FoodItemDto foodItemRequestDto : userOrderDto.getFoodItems()) {
			
			Optional<FoodItem> optionalfooditem = foodItemRepository.findById(foodItemRequestDto.getFoodId());
			
			if (optionalfooditem.isPresent()) {

				FoodItem fooditem = optionalfooditem.get();
				userOrder.setTotalPrice(totalprice);
				userOrder.setUserId(userId);
				userOrder.setOrderDateTime(userDate);
				userOrder.setStatus("success");
				/* userOrderRepository.save(userOrder); */
				 userOrder = userOrderRepository.save(userOrder);
				insertOrderItemdetails(fooditem.getFoodId(), foodItemRequestDto.getQuantity());

				float selecteditemprice = fooditem.getFoodPrice();

				totalprice = totalprice + selecteditemprice * foodItemRequestDto.getQuantity();
				

			}
		}
		
		
		return "you ordered succesfully";

	}

	private void insertOrderItemdetails(Integer foodId, int quantity) {
		OrderItems orderItems = new OrderItems();
		orderItems.setFoodId(foodId);
		orderItems.setOrderId(userOrder.getUserOrderId());
		orderItems.setQuantity(quantity);
		orderItemsRepository.save(orderItems);

	}

	@Override
	public List<UserOrderDto> getOrderList(int userId, int pageNumber, int pageSize) throws UserNotFoundException {
		// TODO Auto-generated method stub
		List<UserOrder> foodOrdersList = new ArrayList<UserOrder>();
		List<UserOrderDto> foodOrderDtoList = new ArrayList<UserOrderDto>();

		Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(Direction.ASC, "foodName"));
		foodOrdersList = userOrderRepository.findByuserId(userId, pageable);
		if (!foodOrdersList.contains(userId)) {
			throw new UserNotFoundException("User Not Found");
		}
		for (UserOrder foodOrders : foodOrdersList) {
			UserOrderDto userdOrderDto = new UserOrderDto();
			BeanUtils.copyProperties(foodOrders, userdOrderDto);
			foodOrderDtoList.add(userdOrderDto);
		}
		return foodOrderDtoList;
	}

	@Override
	public String getInfo() {
		// TODO Auto-generated method stub
		return "Updated";
	}

}
