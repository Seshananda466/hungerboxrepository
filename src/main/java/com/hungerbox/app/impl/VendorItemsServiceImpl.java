package com.hungerbox.app.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hungerbox.app.entity.VendorItems;
import com.hungerbox.app.repository.VendorItemsRepository;
import com.hungerbox.app.service.VendorItemsService;

@Service
public class VendorItemsServiceImpl implements VendorItemsService {
	@Autowired
	VendorItemsRepository vendorItemsRepository;

	@Override
	public String addVendorItems(VendorItems vendorItems) {
		// TODO Auto-generated method stub

		vendorItemsRepository.save(vendorItems);

		return "You selected an item from vendor having Id: " + vendorItems.getVendorId();
	}

}
