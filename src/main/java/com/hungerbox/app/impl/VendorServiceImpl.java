package com.hungerbox.app.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.hungerbox.app.dto.VendorDto;
import com.hungerbox.app.entity.Vendor;
import com.hungerbox.app.exception.VendorNotFoundException;
import com.hungerbox.app.repository.VendorRepository;
import com.hungerbox.app.service.VendorService;

@Service
public class VendorServiceImpl implements VendorService {

	@Autowired
	VendorRepository vendorRepository;

	@Override
	public List<VendorDto> findByvendorNameContains(String vendorName, int pageNumber, int pageSize)
			throws VendorNotFoundException {

		/*
		 * Pageable pageable = PageRequest.of(pageNumber, pageSize,
		 * Sort.by(Direction.ASC, "vendorName")); return
		 * vendorRepository.findByvendorNameContains(vendorName,
		 * pageable).stream().collect(Collectors.toList());
		 */

		List<Vendor> vendorList = new ArrayList<Vendor>();
		List<VendorDto> vendorDtoList = new ArrayList<VendorDto>();

		Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(Direction.ASC, "vendorName"));
		vendorList = vendorRepository.findByvendorNameContains(vendorName, pageable);
		if (vendorList.isEmpty()) {
			throw new VendorNotFoundException("Vendor Not Found");
		}
		for (Vendor vendor : vendorList) {
			VendorDto vendorDto = new VendorDto();
			BeanUtils.copyProperties(vendor, vendorDto);
			vendorDtoList.add(vendorDto);
		}
		return vendorDtoList;

	}

}
