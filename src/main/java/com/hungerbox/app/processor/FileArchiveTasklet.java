package com.hungerbox.app.processor;

import java.io.IOException;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.core.io.Resource;


public class FileArchiveTasklet implements Tasklet {

	private static final Logger LOGGER=LoggerFactory.getLogger(FileArchiveTasklet.class);			
Resource[] resources;


	public Resource[] getResources() {
	return resources;
}


public void setResources(Resource[] resources) {
	this.resources = resources;
}


	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		// TODO Auto-generated method stub
		
		Arrays.asList(resources).forEach(r -> {
			try {
				r.getFile().delete();
			}catch(IOException e) {
				LOGGER.error(e.getMessage());
			}
		});
		return RepeatStatus.FINISHED;
	}

}
