package com.hungerbox.app.processor;

import javax.persistence.Entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import com.hungerbox.app.entity.Vendor;


public class VendorMapper implements FieldSetMapper<Vendor> {
	private static final Logger LOGGER = LoggerFactory.getLogger(VendorMapper.class);

	@Override
	public Vendor mapFieldSet(FieldSet fieldData) throws BindException {
		// TODO Auto-generated method stub

		Vendor vendor = new Vendor();

		vendor.setVendorId(fieldData.readString(1));
		vendor.setVendorName(fieldData.readString(2));
		vendor.setVendorEmail(fieldData.readString(3));
		vendor.setVendorAccountNumber(fieldData.readLong("vendoraccountnumber"));
		vendor.setDescription(fieldData.readString(5));
		vendor.setVendorPhoneNum(fieldData.readString(6));
		vendor.setAddress(fieldData.readString(7));
		LOGGER.info(vendor.toString());
		return vendor;
	}

}
