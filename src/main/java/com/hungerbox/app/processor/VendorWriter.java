package com.hungerbox.app.processor;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

public class VendorWriter<T> implements ItemWriter<T> {

	private static final Logger LOGGER = LoggerFactory.getLogger(VendorWriter.class);

	@Override
	public void write(List<? extends T> items) throws Exception {
		items.forEach(i -> LOGGER.info(i.toString()));

	}

}
