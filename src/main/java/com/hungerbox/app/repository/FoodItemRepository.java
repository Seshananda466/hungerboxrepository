package com.hungerbox.app.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hungerbox.app.entity.FoodItem;

@Repository
public interface FoodItemRepository extends JpaRepository<FoodItem,Integer> {
	
	//@Query("SELECT NEW com.example.demo.dto.FoodItemDto(fid.foodId,fid.foodName,fid.foodDescription,fid.foodPrice) FROM FoodItem as fid where foodName= ?1")
	/*FoodItemDto findByFoodName(String foodName);*/
	public List<FoodItem> findByfoodNameContains(@Param("foodName") String foodName, Pageable pageRequest);

}
