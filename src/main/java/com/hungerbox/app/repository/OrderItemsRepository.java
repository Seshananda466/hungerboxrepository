package com.hungerbox.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hungerbox.app.entity.OrderItems;

@Repository
public interface OrderItemsRepository extends JpaRepository<OrderItems,Integer> {

	/*
	 * @Query(
	 * value="insert  into order_items (order_id,orderi.food_id,orderi.quantity) values(:order_id,:food_id,:quantity)"
	 * ,nativeQuery=true)
	 */
	/* void savee(OrderItems orderItems); */


}
