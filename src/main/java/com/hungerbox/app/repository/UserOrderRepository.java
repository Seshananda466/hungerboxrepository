package com.hungerbox.app.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hungerbox.app.entity.UserOrder;

@Repository
public interface UserOrderRepository extends JpaRepository<UserOrder, Integer> {

	/*
	 * @Query("SELECT NEW com.example.demo.dto.UserOrderDto(uod.userId,uod.foodId,uod.fooditems,uod.quantity,uod.accountNumber) FROM UserOrder as uod where userId= ?1"
	 * )
	 */

	public List<UserOrder> findByuserId(@Param("userId") int userId,Pageable pageRequest);

	public UserOrder findByuserId(String userId);

}
