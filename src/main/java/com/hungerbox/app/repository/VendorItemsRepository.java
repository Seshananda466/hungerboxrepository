package com.hungerbox.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hungerbox.app.entity.VendorItems;

@Repository
public interface VendorItemsRepository extends JpaRepository<VendorItems,String> {





}
