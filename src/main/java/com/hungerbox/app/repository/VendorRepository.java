package com.hungerbox.app.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hungerbox.app.entity.Vendor;

@Repository
public interface VendorRepository extends JpaRepository<Vendor,String> {

	List<Vendor> findByvendorNameContains(String vendorName, Pageable pageable);

	Object findByvendorNameContains(String vendorName, int i, int j);
	

	/*
	 * List<VendorDto> findByvendorNameContains(String vendorName, Pageable
	 * pageable);
	 */


}
