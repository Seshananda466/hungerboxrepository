package com.hungerbox.app.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.hungerbox.app.dto.FoodItemDto;
import com.hungerbox.app.dto.FoodItemRequestDto;
import com.hungerbox.app.dto.UserOrderDto;
import com.hungerbox.app.exception.FoodItemNotFoundException;

public interface FoodItemService {

	List<FoodItemRequestDto> findByfoodNameContains(String foodName, int pageNumber, int pageSize) throws FoodItemNotFoundException;

	

	/*
	 * List<FoodItemDto> findByfoodNameContains(UserOrderDto userOrderDto, int
	 * pageNumber, int pageSize);
	 */

}
