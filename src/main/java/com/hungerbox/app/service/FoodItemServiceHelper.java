package com.hungerbox.app.service;

import org.springframework.stereotype.Component;

import com.hungerbox.app.dto.FoodItemRequestDto;
import com.hungerbox.app.entity.FoodItem;

@Component
public class FoodItemServiceHelper {
	public static FoodItemRequestDto entityToDto(FoodItem foodItem) {
		FoodItemRequestDto foodItemRequestDto = new FoodItemRequestDto(foodItem.getFoodId(),foodItem.getVendorId(),foodItem.getFoodName(),foodItem.getFoodDescription(),foodItem.getFoodPrice());
		return foodItemRequestDto;
}
}