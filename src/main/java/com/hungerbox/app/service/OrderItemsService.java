package com.hungerbox.app.service;

import java.util.List;

import com.hungerbox.app.entity.OrderItems;
import com.hungerbox.app.exception.OrderDetailsNotFoundException;

public interface OrderItemsService {

	List<OrderItems> getOrderItemsByDesc(Integer pageNo, Integer pageSize, String sortByDesc) throws OrderDetailsNotFoundException;

	
}
