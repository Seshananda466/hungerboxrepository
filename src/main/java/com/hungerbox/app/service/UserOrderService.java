package com.hungerbox.app.service;

import java.util.List;

import com.hungerbox.app.dto.UserOrderDto;
import com.hungerbox.app.exception.UserNotFoundException;

public interface UserOrderService {

	public String createOrder(UserOrderDto userOrderDto);

	public List<UserOrderDto> getOrderList(int userId, int pageNumber, int pageSize) throws UserNotFoundException;
	public String getInfo();

}
