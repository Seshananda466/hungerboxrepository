package com.hungerbox.app.service;

import java.util.List;

import com.hungerbox.app.dto.VendorDto;
import com.hungerbox.app.exception.VendorNotFoundException;

public interface VendorService {

	List<VendorDto> findByvendorNameContains(String vendorName, int pageNumber, int pageSize)
			throws VendorNotFoundException;

}
