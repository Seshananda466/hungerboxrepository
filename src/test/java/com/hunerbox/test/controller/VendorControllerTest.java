package com.hunerbox.test.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.ResponseEntity;

import com.hungerbox.app.controller.VendorController;
import com.hungerbox.app.dto.VendorDto;
import com.hungerbox.app.exception.VendorNotFoundException;
import com.hungerbox.app.service.VendorService;

public class VendorControllerTest {

	@Mock
	VendorService vendorService;

	@InjectMocks
	VendorController vendorController;

	static VendorDto vendorDto;

	@BeforeAll
	public static void setup() {
		vendorDto = new VendorDto();
		vendorDto.setVendorId("V00001");
		vendorDto.setVendorName("SRI SAI CATERING");
		vendorDto.setDescription("only for veg meals");
		vendorDto.setAddress("Bangalore");
		vendorDto.setVendorPhoneNum("080235467");
		vendorDto.setVendorEmail("srisaicaters@gmail.com");

	}

	@Test
	@DisplayName( "vendor list:Positive scenrios")
	public void vendorTest() throws VendorNotFoundException {

		List<VendorDto> vendorList = new ArrayList<VendorDto>();
		
		when(vendorService.findByvendorNameContains(vendorDto.getVendorName(), 0, 2)).thenReturn(vendorList);
		
		ResponseEntity<List<VendorDto>>  response = vendorController.getVendorByVendorName(vendorDto.getVendorName(), 0, 2);
		verify(vendorService).findByvendorNameContains(vendorDto.getVendorName(), 0, 2);
		assertEquals(vendorList,response);
	}

}
