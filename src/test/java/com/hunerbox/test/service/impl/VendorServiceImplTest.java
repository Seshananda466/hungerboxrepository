package com.hunerbox.test.service.impl;

import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.hungerbox.app.dto.VendorDto;
import com.hungerbox.app.entity.Vendor;
import com.hungerbox.app.repository.VendorRepository;
import com.hungerbox.app.service.VendorService;

@ExtendWith(MockitoExtension.class)
public class VendorServiceImplTest {

	@Mock
	VendorRepository vendorRepository;

	@InjectMocks
	VendorService vendorService;

	static VendorDto vendorDto;
	/* static VendorDto vendorperst; */
	static 	Vendor vendor;
	@BeforeAll
	public static void setUp() {

		vendorDto = new VendorDto();
		
		vendorDto.setVendorId("V00001");
		vendorDto.setVendorName("SRI SAI CATERING");
		vendorDto.setDescription("only for veg meals");
		vendorDto.setAddress("Bangalore");
		vendorDto.setVendorPhoneNum("080235467");
		vendorDto.setVendorEmail("srisaicaters@gmail.com");
		vendor = new Vendor();
		vendor.setVendorName("SRI SAI CATERING");

	}

	@Test
	@DisplayName("save vendor details")
	public void saveVendorDetails() {

		/*
		 * List<VendorDto> vendorList = new ArrayList<VendorDto>();
		 * 
		 * when(vendorRepository.findByvendorNameContains(vendor.getVendorName(), 0,
		 * 2)).thenReturn(vendorList);
		 * 
		 * ResponseEntity<List<VendorDto>> response =
		 * vendorService.findByvendorNameContains(vendorDto.getVendorName(), 0, 2);
		 * verify(vendorService).findByvendorNameContains(vendorDto.getVendorName(), 0,
		 * 2); assertEquals(vendorList, response);
		 */
		 
	}

}
